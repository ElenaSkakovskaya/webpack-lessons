'use strict';

const webpack = require('webpack');

const NODE_ENV = process.env.NODE_ENV || "development";

console.log(NODE_ENV);

module.exports = {
  mode: 'none',
  entry: "./home",
  output: {
    path: __dirname,
    filename: "bundle.js",
    library: "home"
  },
  watch: NODE_ENV == "development",
  watchOptions: {
    aggregateTimeout: 100
  },
  devtool: process.env.NODE_ENV == 'development' ? "eval" : false, //"cheap-inline-module-source-map"
  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(NODE_ENV)
    }),
  ]
};
"Learning Webpack" 

Source: https://learn.javascript.ru/screencast/webpack

## Useful info:

### Modules out of the bundle
If you want to use some of your modules functionality from html or from other files (which wasn't included in the bundle) you need 
- to make export from module you want to associate with this functionality. This module must declare or import this functionality, no matter
- to include module name in output.library option in webpack

### Source mapping
https://webpack.js.org/configuration/devtool/

'eval' is optimal for development

'inline-cheap-module-source-map': inline option includes mapping to bundle file and doesn't create mapping file


### Environment
EnvironmentPlugin or DefinePlugin let you use process.env properties in js-code for browser
 
Using process.env.NODE_ENV is useful in defining webpack config
